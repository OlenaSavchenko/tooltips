/**
 * Написати функціонал для тултипів
 * 
 * Налаштування:
 * - текст
 * - позиціонування (зверху, справа, знизу, зліва), яке за замовчуванням зверху
 * - можливості відображення (при наведенні чи при кліку), за замовчуванням при наведенні
 *  
 */

class Tooltip {
    constructor(targetSelector, text, placement = 'top') {
        this.targetElement = document.querySelector(targetSelector);
        this.isDisabled = false;
        this.targetElement.classList.add('tooltip');
        this.placement = placement;
        this.text = text;
        this.render();
        this.classMap = {
            top: 'tooltip-up',
            right: 'tooltip-right',
            bottom: 'tooltip-down',
            left: 'tooltip-left',
        }
    }
    render() {
        this.tooltip = document.createElement('span');
        this.tooltip.classList.add('tooltip-content');
        this.tooltip.textContent = this.text;
        this.targetElement.prepend(this.tooltip);
    }
    show() {
        if (this.isDisabled) return;
        this.tooltip.classList.add('active');
        this.targetElement.classList.add(this.getPositionClassName());
    }

    hide() {
        this.tooltip.classList.remove('active');
    }
    toggle() {
        if (this.isDisabled) return;
        this.tooltip.classList.toggle('active');
        this.targetElement.classList.add(this.getPositionClassName());
    }
    getPositionClassName() {
        return this.classMap[this.placement];
    }
    disable() {
        this.hide();
        this.isDisabled = true;
    }

    enable() {
        this.isDisabled = false;
    }
    //ASK--------------should be import to FocusTooltip?
    changeText(newText) {
        this.tooltip.textContent = newText;
    }
    setPosition(position) {
        const posArr = Object.keys(this.classMap)
        if (!posArr.includes(position)) return
        this.targetElement.classList.remove(this.getPositionClassName());
        this.placement = position;
        this.targetElement.classList.add(this.getPositionClassName());
    }
}


class HoverTooltip extends Tooltip {
    constructor(targetSelector, text, placement) {
        super(targetSelector, text, placement)
        this.attachEvents()
    }
    attachEvents() {
        this.targetElement.addEventListener('mouseover', () => {
            this.show();
        })

        this.targetElement.addEventListener('mouseleave', () => {
            this.hide();
        })
    }
}
class FocusTooltip extends Tooltip {
    constructor(targetSelector, text, placement, inputEl) {
        super(targetSelector, text, placement)
        this.inputEl = inputEl
        this.attachEvents()
    }
    attachEvents() {
        this.inputEl.addEventListener("focus", () => {
            this.show()
        })
        this.inputEl.addEventListener("blur", () => {
            this.hide()
        })
    }
}

class ClickTooltip extends Tooltip {
    constructor(targetSelector, text, placement) {
        super(targetSelector, text, placement)
        this.attachEvents()

    }
    attachEvents() {
        this.targetElement.addEventListener('click', this.toggle.bind(this))
    }
}
//----Buttons---------
const showFirstBtnEl = document.querySelector('#show-first');
const hideFirstBtnEl = document.querySelector('#hide-first');
const disableThirdBtnEl = document.querySelector('#disable-third');
const enableThirdBtnEl = document.querySelector('#enable-third');
const disableAllEl = document.querySelector('#disable-all');
const enableAllEl = document.querySelector('#enable-all');
const applyTextBtnEl = document.querySelector("#apply-text");
const applyAlignBtnEl = document.querySelector("#apply-align");
//----Inputs---------
const applyTextInputEl = document.querySelector("#text-input");
const applyAlignInputEl = document.querySelector("#align-input");
const inputEl = document.querySelector("#input input");
//----Tooltips---------
const firstTooltip = new HoverTooltip('#btn-1', 'It\'s first button');
const secondTooltip = new HoverTooltip('#btn-2', 'It\'s second button', 'right');
const thirdTooltip = new HoverTooltip('#btn-3', 'It\'s third button', 'bottom');
const fourthTooltip = new ClickTooltip('#btn-4', 'It\'s fourth button', 'left');
const inputTooltip = new FocusTooltip('#input', 'It\'s input', 'bottom', inputEl);
const tooltips = [firstTooltip, secondTooltip, thirdTooltip, fourthTooltip, inputTooltip]
//----Listeners---------
showFirstBtnEl.addEventListener('click', () => {
    firstTooltip.show();
})
hideFirstBtnEl.addEventListener('click', () => {
    firstTooltip.hide();
})
disableThirdBtnEl.addEventListener('click', () => {
    thirdTooltip.disable();
})
enableThirdBtnEl.addEventListener('click', () => {
    thirdTooltip.enable();
})
disableAllEl.addEventListener('click', () => {
    tooltips.forEach(tooltip => tooltip.disable());
})
enableAllEl.addEventListener('click', () => {
    tooltips.forEach(tooltip => tooltip.enable());
})
applyTextBtnEl.addEventListener("click", () => {
    secondTooltip.changeText(applyTextInputEl.value)
    applyTextInputEl.value = "";

})
applyAlignBtnEl.addEventListener("click", () => {
    fourthTooltip.setPosition(applyAlignInputEl.value)
    applyAlignInputEl.value = "";
})
